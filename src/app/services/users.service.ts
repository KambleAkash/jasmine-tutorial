import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  url: string = 'https://fakerapi.it/api/v1/users?_quantity=10'; 

  constructor(
    private httpClient: HttpClient
  ) { }

  getUsers(): Observable<any> {
    return this.httpClient.get(this.url);
  }
}