import { Component, OnInit } from '@angular/core';
import { UsersService } from './services/users.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'jasmine-tutorial';
  users: any[] = [];

  constructor(
    private userService: UsersService
  ) {}

  ngOnInit(): void {
    this.getUsers();
  }

  getUsers(): void {
    this.userService.getUsers().subscribe({
      next: (users: {data: any[]}) => {
        this.users = users.data.map(user => {
          return {
            name: `${user?.firstname} ${user?.lastname}`,
            email: user?.email,
            isVerified: false
          }
        })
      },
      error: (error) => {
        console.error(error);
      }
    })
  }

  markUsersAsVerified(): void {
    this.users.forEach(user => {
      user.isVerified = true;
    })
  }
}
