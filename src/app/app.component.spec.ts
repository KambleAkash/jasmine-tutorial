import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { UsersService } from './services/users.service';
import { getMockUsers } from 'src/testing/test-data/mock-users';
import { of, throwError } from 'rxjs';

describe('AppComponent', () => {
  let appComponent: AppComponent;
  let appComponentFixture: ComponentFixture<AppComponent>;
  let userServiceSpyObject = jasmine.createSpyObj(
    'UsersService',
    ['getUsers']
  );

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      imports: [],
      providers: [
        {
          provide: UsersService,
          useValue: userServiceSpyObject
        }
      ]
    }).compileComponents();
    appComponentFixture = TestBed.createComponent(
      AppComponent
    ); 
    appComponent = appComponentFixture.componentInstance;
    userServiceSpyObject = TestBed.inject(UsersService);
    userServiceSpyObject.getUsers.and.returnValue(of(getMockUsers()));
  });

  it('should create the app', () => {
    expect(appComponent).toBeTruthy();
  });

  describe('ngOnInit() spec suite', () => {
    it('should call getUsers()', () => {
      // Arrange
      spyOn(appComponent, 'getUsers');
      // Action
      appComponent.ngOnInit();
      // Assert 
      expect(appComponent.getUsers).toHaveBeenCalled();
    });
  })

  describe('getUsers() spec suite', () => {
    it('should map users', () => {
      // Arrange
      appComponent.users = [];
      // Action
      appComponent.getUsers();
      // Assert
      expect(appComponent.users).toEqual([
        {
          name: "User 1 Temp",
          email: "user1@email.com",
          isVerified: false
        },
        {
          name: "User 2 Temp",
          email: "user2@email.com",
          isVerified: false
        }
      ]);
    });

    it('should handle error while fetching users', () => {
      // Arrange
      userServiceSpyObject.getUsers.and.returnValue(throwError(() => "Error"));
      spyOn(console, 'error');
      // Action
      appComponent.getUsers();
      // Assert
      expect(console.error).toHaveBeenCalledWith("E");
    });
  });

  describe('markUsersAsVerified() spec suite', () => {
    it('should verfiy all users', () => {
      // Arrange
      appComponent.users = [
        {
          isVerified: false
        }
      ];
      // Action
      appComponent.markUsersAsVerified();
      // Assert
      expect(appComponent.users).toEqual([
        {
          isVerified: true
        }
      ])
    })
  });
});
