export function getMockUsers() {
    return {
        data: [
            {
                firstname: "User 1",
                lastname: "Temp",
                email: "user1@email.com"
            },
            {
                firstname: "User 2",
                lastname: "Temp",
                email: "user2@email.com"
            }
        ]
    } 
    
}